# Installation

Install like any other Drupal module.


# Documentation

See <https://www.drupal.org/documentation/modules/big_pipe>.


# Environment requirements

See <https://www.drupal.org/documentation/modules/big_pipe/environment>.
